let mix = require('laravel-mix');

mix.styles([
	'node_modules/materialize-css/dist/css/materialize.css'
],'public/css/app.css');

mix.scripts([
	'node_modules/jquery/dist/jquery.js',
	'node_modules/materialize-css/dist/js/materialize.js'
],'public/js/app.js');

mix.copyDirectory('node_modules/materialize-css/dist/fonts','public/fonts');