const Path = require('path');

module.exports = {
	entry: './main.js',
	output: {
		path: Path.resolve(__dirname,'./public/javascripts'),
		filename: 'app.js'
	}
}